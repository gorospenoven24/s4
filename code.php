<?php

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;

    }
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getFloor() {
        return $this->floors;
    }

    public function getAddress() {
        return $this->address;
    }
}

class Condominium extends Building {
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getFloor() {
        return $this->floors;
    }

    public function getAddress() {
        return $this->address;
    }
}



$building = new Building('casswynn Building', 8, 'Timog avenue Quexon city');
$condominium = new Condominium ('Enzon' ,5 , 'Buendia');
